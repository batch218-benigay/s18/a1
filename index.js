console.log("s18 Activity")
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.*/

		function addition(num1, num2){
			console.log("Displayed sum of " + num1 + " and " + num2);
			console.log(num1+num2);
		}

		/*Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.*/

		function subtraction(num3, num4){
			console.log("Displayed difference of " +num3+ " and " + num4);
			console.log(num3-num4);
		}


		/*-invoke and pass 2 arguments to the addition function*/
		addition(5,15);
		
		/*-invoke and pass 2 arguments to the subtraction function*/
		subtraction(20,5);
		

	/*2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	/*Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.*/

			function product(numA, numB){
				let prod = numA * numB;
				console.log("The product of " +numA+ " and " +numB+ ":");
				return prod;
			}
			let ans1 = product(50,10);

		/*Log the value of product variable in the console.*/			
			console.log(ans1);

		/*Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.*/

			function quotient(numX, numY){
				let quot = numX / numY;
				console.log("The quotient of " +numX+ " and " +numY+ ":");
				return quot;
			}
			let ans2 = quotient(50,10);

		/*Log the value of quotient variable in the console.*/
			console.log(ans2);

	/*3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.*/

			function areaOfCircle(pi, rad){
				let aoc = pi * (rad*rad);
				console.log("The result of getting the area of a circle with " + rad + " radius:");
				ans3 = aoc.toFixed(2);
				return ans3
			}

		/*Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.*/
			let circleArea = areaOfCircle(3.141592, 15);
			
	/*Log the value of the circleArea variable in the console.*/
			console.log(circleArea);

	/*4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.*/

			function overAllAverage(ave1, ave2, ave3, ave4){
				let average = (ave1 + ave2 + ave3 + ave4) / 4;
				console.log("The average of " +ave1+ "," +ave2+ "," +ave3+ ", and " +ave4+ ":");
				return average;
			}

	    /*Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation*/
			let averageVar = overAllAverage(20, 40, 60, 80);

			/*-Log the value of the averageVar variable in the console.*/
			console.log(averageVar);

	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.*/

			function checkScore(score,total){
				let checker = (score/total)*100;
				let isPassed = checker >= 75;
				console.log("Is " +score+ "/" +total+ " a passing score?");
				return isPassed; 
			}

		/*Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.*/
			let grade = checkScore(38,50);
			/*-Log the value of the isPassingScore variable in the console.*/
			console.log(grade);